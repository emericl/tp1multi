package modele;

import java.util.Scanner;

import app.NombrePremiers;

public class Calcul {
	public final static int NOMBRE_DE_NOMBRE_PREMIER_POUR_NOTIFICATION = 20;

	private static int nombreDeNombrePremierTrouve = 0;

	
	
	/**
	 * Retourne le nombre de nombres premiers contenus entre les bornes passées
	 * en paramètres. Retourne 0 s'il n'y a pas de nombre premier entre les
	 * bornes
	 * 
	 * @param minimum
	 *            La borne inférieure
	 * @param maximum
	 *            La borne Supérieure
	 * @return le nombre de nombres premiers
	 */
	public NombrePremiers retourneNombreDeNombrePremierEntre(int minimum, int maximum) {
		NombrePremiers reponse = new NombrePremiers();

		// On recherche les nombres premiers
		for (int i = minimum; i < maximum; i++) {
			if (estPremier(i)) {
				reponse.ajoute(i);
				nombreDeNombrePremierTrouve++;
			}
		}
		return reponse;
	}

	/**
	 * Indique si le nombre est premier
	 * 
	 * @param number
	 *            Le nombre à évaluer
	 * @return vrai si le nombre est premier
	 */
	public boolean estPremier(int number) {
		boolean retVal = true;
		for (int i = 2; i < number; i++) {
			if (number % i == 0)
				retVal = false;
		}
		return retVal;
	}

	/**
	 * Retourne le nombre de nombres premiers trouvés
	 * 
	 * @return le nombre nombres
	 */
	public static int getNombreDeNomrePremier() {
		return nombreDeNombrePremierTrouve;
	}

	/**
	 * Incrémente le nombre total de nombre premier trouver par l'ensemble des
	 * classes.
	 */
	private void incrementeNombreDeNombrePremier() {
		nombreDeNombrePremierTrouve++;
	}

	/**
	 * Démarre l'application
	 */
	private void start() {

		Scanner entree = new Scanner(System.in);
		boolean recommancer = true;
		while (recommancer) {
			// On demande les information à l'utilisateur
			System.out.println("entrez une borne minimale");
			int min = entree.nextInt();
			System.out.println("entrez une borne maximale");
			int max = entree.nextInt();

			// On fait les calculs
			NombrePremiers resultat = this.retourneNombreDeNombrePremierEntre(min, max);

			// On affiche le nombre de nombres
			System.out.println("Il y a " + resultat.getNombreDeNombresPremiers() + " nombres trouvés");
			// On affiche les résultats
			System.out.println("Voici les nombres trouvés");
			System.out.println(resultat.toString());

			// Est-ce qu'on doit recommancer
			System.out.println("voulez-vous recommancez o ou n");
			String choix = entree.next();
			recommancer = choix.compareToIgnoreCase("o")==0 ? true : false;
		}
		System.out.println("Au revoir!");
		System.exit(0);

	}

	/**
	 * fonction principale pour lancer le programme
	 * 
	 * @param args
	 *            inutilisé
	 */
	public static void main(String[] args) {

		new Calcul().start();

	}

}
