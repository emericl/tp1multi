package controleur;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import vue.exercise2Vue;

public class ControleurExercise2 extends Application {
	
	@FXML
	private BorderPane root;
	
	private exercise2Vue vue;
	
	
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		root = FXMLLoader.load(getClass().getResource("/exempleVrai.fxml"));
		//vue = new exercise2Vue(new URL("U:/Workspace/exercice1/src/main/resourcesexempleVrai.fxml"), new ResourceBundles())
		Scene scene = new Scene(root, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("FXML-JavaFX");
		primaryStage.show();
	}
	
	

	public static void main(String[] args) {
		Application.launch(args);
	}
}
