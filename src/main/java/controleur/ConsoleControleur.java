package controleur;

import app.NombrePremiers;
import app.Tutoriel;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import modele.Calcul;

public class ConsoleControleur extends Application {
	Calcul model = new Calcul();
	Tutoriel vue = new Tutoriel();
	
	
	
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		vue.start(primaryStage);		
		ajouterEcouteur();
		
	}
	public static void main(String[] args) {
		launch(args);
	}
	
	private void ajouterEcouteur(){
		vue.getButton().setOnAction(new ecouteur());
	}
	
	private class ecouteur implements EventHandler<ActionEvent>{
		@Override
		public void handle(ActionEvent event) {
			if (event.getSource() == vue.getButton()){
			
				NombrePremiers reponse = model.retourneNombreDeNombrePremierEntre(vue.getTextMin(), vue.getTextMax());
				vue.addNbrViewList(reponse.getTousLesNombres(),reponse.getNombreDeNombresPremiers());
				
			}
				
		}
	}
	
}
