package app;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class Tutoriel extends Application {

	private static boolean SHOW_BORDER = true;
	public final static Border BORDER = new Border(
			new BorderStroke(new Color(0, 0, 1, 1.0), BorderStrokeStyle.SOLID, null, null));
	private static final long serialVersionUID = 1L;

	private TextField minTextField;
	private TextField maxTextField;
	private TextField nombreDeNombresTextField;
	private ListView<Integer> resultList;
	private Pane root;
	private static ObservableList<Integer> nbr = FXCollections.observableArrayList();

	private Button calculButton;

	/**
	 * Create the frame.
	 * 
	 * @param primaryStage
	 * @return
	 */

	public void buildStage(Stage primaryStage) {
		constructUsingFlowPane(primaryStage);
	}

	private void constructUsingFlowPane(Stage primaryStage) {
		root = new FlowPane();

		calculButton = new Button();
		calculButton.setText("rechercher");
		
		
		
		

		Label minLabel = new Label("minimum");
		minTextField = new TextField();

		Label maxLabel = new Label("maximum");
		maxTextField = new TextField();

		Label resultatLabel = new Label("résultats");
		nombreDeNombresTextField = new TextField();
		nombreDeNombresTextField.setDisable(true);

		resultList = new ListView<>();
		resultList = new ListView<Integer>();
		resultList.setEditable(true);
		resultList.setItems(nbr);

		root.getChildren().addAll(minLabel, minTextField, maxLabel, maxTextField, calculButton, resultatLabel,
				nombreDeNombresTextField, resultList);

		Scene scene = new Scene(root, 500, 280);

		primaryStage.setTitle("Chercheur de nombres premiers");
		primaryStage.setScene(scene);

	}

	public Button getButton() {
		return calculButton;
	}

	public int getTextMin() {
		if (minTextField.getText() == "") {
			return 0;
		} else {
			return Integer.parseInt(minTextField.getText());
		}
	}

	public int getTextMax() {
		if (maxTextField.getText() == "") {
			return 0;
		} else {
			return Integer.parseInt(maxTextField.getText());
		}
	}
	
	
	
	public void addNbrViewList(Integer[] tab,int size){
		nbr.clear();
		for(int i = 0; i<size;i++){
			nbr.add(tab[i]);
		}
		
		resultList.refresh();
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		buildStage(primaryStage);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}
