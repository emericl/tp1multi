package app;

import java.util.LinkedList;
import java.util.List;

/**
 * Classe qui contient une série de nombres
 * 
 * @author martin
 */
public class NombrePremiers {
	private List<Integer> nombres;

	public NombrePremiers() {
		// TODO Pourquoi prend-t-on une linkedlist au lieu d'une arraylist ?
		nombres = new LinkedList<Integer>();
	}

	public void ajoute(Integer nombre) {
		nombres.add(nombre);
	}

	/**
	 * Retourne tous les nombres premiers trouvés
	 * 
	 * @return un tableau contenant tous les nomrbes
	 */
	public Integer[] getTousLesNombres() {
		// TODO est-ce que le LinkedList est aussi intéressant ici ?
		return nombres.toArray(new Integer[0]);
	}

	/**
	 * Retourne le nombre de nombres premiers trouvé
	 * 
	 * @return
	 */
	public Integer getNombreDeNombresPremiers() {
		return nombres.size();
	}

	/**
	 * Retourne le nombre qui se trouve à l'indice passé en paramètre
	 * 
	 * @param index
	 *            l'indice où rechercher le nombre
	 * @return le nombre recherché
	 */
	public Integer retourneNombreAt(int index) {
		return nombres.get(index);
	}
	
	@Override
	public String toString(){
		return nombres.toString();
	}

}
